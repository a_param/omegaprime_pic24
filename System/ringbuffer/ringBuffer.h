/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    ============================================================================
    This document contains proprietary information belonging to 
    Achuthan Paramanathan. Passing on and copying of this document, use and 
    communication of its contents is not permitted without prior written 
    authorisation.
    ============================================================================
    Revision Information:
        File name: ringBuffer.h
        Version:   v0.0
        Date:      2012-08-09
    ============================================================================
 */

/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/



#include "../system.h"


/*
** =============================================================================
**                       EXPORTED FUNCTION DECLARATION
** =============================================================================
*/

int buffer_full(rBufPar *bPar);
int buffer_empty(rBufPar *bPar);
void write_buffer( uint8_t *dPtr, rBufPar *bPar);
void read_buffer(uint8_t *dPtr, rBufPar *bPar);

