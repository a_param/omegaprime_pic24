/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: ringBuffer.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include "ringBuffer.h"


/*
** =============================================================================
**                                   GLOBAL VARIABLES
** =============================================================================
*/

rBufPar rBuf;


/*
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */


/*==============================================================================
 ** Function...: ringBuffer_init
 ** Return.....: void
 ** Description: Ring buffer initialization
 ** Created....: 23.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void ringBuffer_init(void)
{
}



/*==============================================================================
 ** Function...: buffer_full
 ** Return.....: int, return true if full
 ** Description: is buffer full?
 ** Created....: 23.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
int buffer_full(rBufPar *bPar)
{
  char s[96];

  // Buffer size exceeds the defined total size
  if(bPar->data_size > BUFFER_SIZE )
  {
    sprintf(s,"LOG_ERROR 2\n");
    uart_print(s);
  }
  // Buffer size is below 0
  else if(bPar->data_size < 0 )
  {
    sprintf(s,"LOG_ERROR 3\n");
    uart_print(s);
  }
  // Read and write pointer exceeds the defined total size
  if(bPar->read_pointer != bPar->write_pointer && bPar->data_size ==  BUFFER_SIZE)
  {
    sprintf(s,"LOG_ERROR 4\n");
    uart_print(s);
  }
  
  return bPar->read_pointer == bPar->write_pointer &&
    bPar->data_size ==  BUFFER_SIZE;
}


/*==============================================================================
 ** Function...: buffer_empty
 ** Return.....: int, true if empty 
 ** Description: is buffer empty?
 ** Created....: 23.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
int buffer_empty(rBufPar *bPar)
{
  char s[96];

  // Buffer size exceeds the defined total size
  if(bPar->data_size > BUFFER_SIZE) 
  {
    sprintf(s,"LOG_ERROR 6\n");
    uart_print(s);
  }
  // Buffer size is below 0
  else if(bPar->data_size < 0 )
  {
    sprintf(s,"LOG_ERROR 7\n");
    uart_print(s);
  }
  if(bPar->read_pointer != bPar->write_pointer && bPar->data_size == 0) 
  {
    sprintf(s,"LOG_ERROR 8\n");
    uart_print(s);
  }
    
  return bPar->read_pointer == bPar->write_pointer &&
  bPar->data_size == 0;
}


/*==============================================================================
 ** Function...: write_buffer
 ** Return.....: void
 ** Description: Write to the ring buffer
 ** Created....: 23.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void write_buffer( uint8_t *dPtr, rBufPar *bPar)
{
  char s[96];

  // Buffer size exceeds the defined total size
  if(bPar->data_size > BUFFER_SIZE)
  {
    sprintf(s,"LOG_ERROR 10\n");
    uart_print(s);
  }

  if (++bPar->write_pointer >= BUFFER_SIZE) bPar->write_pointer = 0;
  memcpy(bPar->buffer[bPar->write_pointer], dPtr, PACKET_SIZE);
  

  if(bPar->data_size >= BUFFER_SIZE )
  {
      sprintf(s,"LOG_ERROR 11\n");
      uart_print(s);
  }
  else
  {
      bPar->data_size++;
  }

}


/*==============================================================================
 ** Function...: read_buffer
 ** Return.....: void
 ** Description: Read from ring buffer
 ** Created....: 23.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void read_buffer(uint8_t *dPtr, rBufPar *bPar)
{
  char s[96];
  if(bPar->data_size > BUFFER_SIZE)
  {
      sprintf(s,"LOG_ERROR 12\n");
      uart_print(s);
  }
  if(!buffer_empty(bPar))
  {
    if (++bPar->read_pointer >= BUFFER_SIZE) bPar->read_pointer = 0;
    if(bPar->data_size <= 0)
    {
                
    }
    else
    {
      bPar->data_size--;
    }
    memcpy(dPtr, bPar->buffer[bPar->read_pointer], PACKET_SIZE);
    memset(bPar->buffer[bPar->read_pointer], '0',PACKET_SIZE );
  }
  else
  {
    uart_print("INVALID READ BUF!\n");
  }
}