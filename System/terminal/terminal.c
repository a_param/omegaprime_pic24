/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: terminal.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include "pps.h"
#include <uart.h>
#include <stdint.h>

#include "../system.h"
#include "../../Drivers/driver.h"

/*
 ** ============================================================================
 **                           LOCAL DEFINES
 ** ============================================================================
 */
#define MAX_ARGV 40


/*
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */
uint16_t parseArgs(char *str, char *argv[]);


/*
 ** ============================================================================
 **                       LOCAL VARIABLES 
 ** ============================================================================
 */
uint16_t argc;
char *argv[MAX_ARGV];


/*==============================================================================
 ** Function...: terminal
 ** Return.....: void 
 ** Description: Parse userinputs 
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void terminal(void *msg) {
  char s[100];
  char cmdStr[MAX_ARGV];
  
  memcpy(cmdStr, msg, MAX_ARGV);
  
  argc = parseArgs(cmdStr, argv);

  if (argc) {
    if (strcmp(argv[0], "help") == 0) {
      // list the commands supported
      uart_print("The following commands are supported:\n");
      uart_print("Version\n");
      uart_print("spiDebug on/Off \n");
      uart_print("tbd \n");
    } else {
      sprintf(s, "Error: unknown cmd: %s\n", argv[0]);
      uart_print(s);
    }
  }
}


/*==============================================================================
 ** Function...: parseArgs
 ** Return.....: void 
 ** Description: Parse userinputs 
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
uint16_t parseArgs(char *str, char *argv[]) {
  uint16_t i = 0;
  char *ch = str;

  if (*ch == '\r') {
    return 0;
  }

  while (*ch != '\0') {
    i++;

    /*Check if length  exceeds*/
    if (i > MAX_ARGV) {
      uart_print("Too many arguments\n");
      return 0;
    }
    argv[i - 1] = ch;
    while (*ch != ' ' && *ch != '\0' && *ch != '\r' && *ch != '\n')
    {
      if (*ch == '"') // Allow space characters inside double quotes
      {
        ch++;
        argv[i - 1] = ch; // Drop the first double quote char
        while (*ch != '"') {
          if (*ch == '\0' || *ch == '\r') {
            uart_print("Syntax error\n");
            return 0;
          }
          ch++; // Record until next double quote char
        }
        break;
      } else {
        ch++;
      }
    }
    if (*ch == '\r') {
      break;
    }
    if (*ch != '\0') {
      *ch = '\0';
      ch++;
      while (*ch == ' ') {
        ch++;
      }
    }
  }
  return i;
}