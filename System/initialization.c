/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: app_uart_test.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */


#include "../Includes/globalInclude.h"
#include "../System/system.h"
#include "../Protocol/protocol.h"
#include "../Drivers/driver.h"




char rxClockOutSize = 0;


/*
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */

#define START_ONE_MILI_TIMER  oneMilliTimer    = 0;

/*==============================================================================
 ** Function...: systemInit
 ** Return.....: void
 ** Description: Main init function
 ** Created....: 25.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void systemInit(void)
{
    AD1PCFGL     = 0xFFFF; // Set to all digital I/O
    
    TRISAbits.TRISA0 = 0; 
    TRISAbits.TRISA1 = 0; 
    TRISAbits.TRISA2 = 0; 
    
    UART1Init();
    uart_print("UART Initialized\n");
    
    uart_print("Initializing Time Driver .........");
    timer();
    uart_print("Done\n");
    
    uart_print("Initializing SPI Driver .........");
    spi();
    uart_print("Done\n");
    
    uart_print("Initializing NRF905 Driver .........");
    nrf();
    setNRFpw(TX, PACKET_SIZE);
    uart_print("Done\n");
    
    uart_print("Initializing Network protocol [NET LAYER] .........");
    opmInit();
    uart_print("Done\n");
    uart_print("Initializing Network protocol [MAC LAYER] .........");
    dcfInit();
    uart_print("Done\n");
    
    uart_print("System initialization complete\n");
    
    
}