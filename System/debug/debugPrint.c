/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: debugPrint.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include "../../Drivers/driver.h"
#include "../../Includes/globalInclude.h"



/*==============================================================================
 ** Function...: op_hexdump
 ** Return.....: void
 ** Description: 
 ** Created....: 29.12.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/

void op_hexdump(int level, const char *title, const void *buf, char len)
{
    char s[40];
    int i = 0, j = 0;
    char d[32] = { 0 };
    
    memcpy(d, buf, len);
    
    sprintf(s,"%s", title);
    uart_print(s);
    
    uart_print("\n");
	for (i = 0; i < len; i++)
	{
            sprintf(s," 0x%02x", d[i]);
            uart_print(s);
            j++;
            if (j == 8)
            {
                j = 0;
                uart_print("\n");
            }
	}
	uart_print("\n\n");
}