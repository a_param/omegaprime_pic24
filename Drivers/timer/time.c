/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
        Revision Information:
        File name: timer.c
        Version:   v0.0
        Date:      21-05-2015
    ============================================================================
 */

/*============================================================================*/
/*                           INCLUDE STATEMENTS
==============================================================================*/

#include <stdio.h>
#include "time.h"

/*
** =============================================================================
**                                   GLOBAL VARIABLES
** =============================================================================
*/

// For Timer 1
unsigned int time_tik = 0;
unsigned int oneMilliTimer = 0;
unsigned int oneTikTimer = 0;
unsigned long long longCounter_MS = 0;
unsigned int difsTimer = 0;
unsigned int ackTimer = 0;
unsigned int slotTimer = 0;

// For Timer 2
unsigned int txPktTimer = 0;


/*------------------------------------------------------------------------------
** Function...: timer
** Return.....: void
** Description: Initialize timer interrupt
** Created....: 09.01.2012 by Simon
** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/

void timer(void)
{   
   PR1 = 0x64;
  
   T1CONbits.TSIDL = 0;  //Continue module operation in Idle mode
   T1CONbits.TGATE = 0;  //Gated time accumulation is disabled
   T1CONbits.TCKPS = 0b01; //scale 1:8
   T1CONbits.TSYNC = 0;    //Do not synchronize external clock input
   T1CONbits.TCS   = 0;  //Internal clock (FOSC/2)

   
           
   IPC0bits.T1IP   = 5;	 //set interrupt priority (Between 1 and 7)
   IFS0bits.T1IF   = 0;	 //reset interrupt flag
   IEC0bits.T1IE   = 1;	 //turn on the timer1 interrupt
   T1CONbits.TON   = 1;  //Start timer
   
   IPC1bits.T2IP   = 7;
   IFS0bits.T2IF   = 0;
   IEC0bits.T2IE   = 1;
   T2CONbits.TON   = 1;

}


/*------------------------------------------------------------------------------
** Function...: timer_1 ISR
** Return.....: void
** Description: ISR
** Created....: 09.08.2012 by Achuthan
** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
void __attribute__((__interrupt__, auto_psv)) _T1Interrupt(void)
{
    time_tik++;
    oneTikTimer++;
    difsTimer++;
    slotTimer++;
    ackTimer++;
    
    if(oneTikTimer>60000)
    {
        oneTikTimer=0;
    }
    
    /* one millisecond */
    if(time_tik >= 10) 
    {
        time_tik = 0;
        oneMilliTimer++; // millisecond counter
        longCounter_MS++; //long count milliseconds
    }
    
    IFS0bits.T1IF = 0;
}


/*------------------------------------------------------------------------------
** Function...: timer_2 ISR
** Return.....: void
** Description: ISR
** Created....: 04.01.2016 by Achuthan
** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/

void __attribute__((__interrupt__, auto_psv)) _T2Interrupt(void)
{
    txPktTimer++;
    
    /* one millisecond */
    if(txPktTimer >= 10) 
    {
        //char s[40];
        //sprintf(s," Hello from timer 2 \n");
        //uart_print(s);
        timer2_cb();
        txPktTimer = 0;
    }
    IFS0bits.T2IF = 0; //Reset Timer2 interrupt flag and Return from ISR
}