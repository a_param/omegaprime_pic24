/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: time.h
        Version:   v1.0
        Date:      25-04-2015
    =========================================================================
*/

/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#include <p24Fxxxx.h>
#include "../driver.h"
/*
** =============================================================================
**                       EXPORTED FUNCTION DECLARATION
** =============================================================================
*/

#ifndef TIMER_H
#define	TIMER_H

typedef void (*cb_timer)(void);
void register_timer_cb(cb_timer ptr_reg_cb);
void timer2_cb(void);

/**
 * Description: initializes the timer
 * @param: void
 * @return: global return code
*/
void timer(void);

/**
 * Description: timer interrupt
*/
void __attribute__((__interrupt__, auto_psv)) _T1Interrupt(void);


#endif	/* TIMER_H */