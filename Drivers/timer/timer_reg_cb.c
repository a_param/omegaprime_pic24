/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
        Revision Information:
        File name: timer_reg_cb.c
        Version:   v0.0
        Date:      06-01-2016
    ============================================================================
 */

/*============================================================================*/
/*                           INCLUDE STATEMENTS
==============================================================================*/

#include <stdio.h>
#include "time.h"

/*
 ** ============================================================================
 **                   LOCAL VARIABLES
 ** ============================================================================
 */

static cb_timer ptr_reg_tx_cb;
static uint8_t tx_cb_flag = 0;


/*------------------------------------------------------------------------------
** Function...: register_timer_tx_cb
** Return.....: void
** Description: registration of timer callback functions
** Created....: 06.01.2016 by Simon
** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
void register_timer_cb(cb_timer ptr_reg_cb)
{
    ptr_reg_tx_cb = ptr_reg_cb;
    tx_cb_flag = 1;
    uart_print("register_timer_cb done\n");                                
}


/*------------------------------------------------------------------------------
** Function...: timer_tx_cb
** Return.....: void
** Description: calling the tx cb function
** Created....: 06.01.2016 by Simon
** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
void timer2_cb(void)
{
    if(tx_cb_flag)
    {
        (*ptr_reg_tx_cb)();  
    }else
    {
        uart_print("ERROR:  timer_tx_cb not registered\n");    
    }
}

