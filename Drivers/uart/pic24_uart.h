/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: uart.h
        Version:   v0.0
        Date:      21-05-2015
    ============================================================================
 */

#ifndef UART_H
#define	UART_H

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */



void UART1Init(void);
void uart_print(char* ToBeSent);

#endif //UART_H