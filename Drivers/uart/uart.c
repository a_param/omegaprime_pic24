/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
    File name: uart.c
    Version:   v0.0
    Date:      21-05-2015
================================================================================
 */


/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include "pps.h"
#include <uart.h>
#include <stdint.h>

#include "pic24_uart.h"
#include "../../System/system.h"


/*
 ** ============================================================================
 **                           LOCAL DEFINES
 ** ============================================================================
 */
#define MAX_ARGV 40

/*
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */


/*
 ** ============================================================================
 **                       LOCAL VARIABLES 
 ** ============================================================================
 */

char USBbuffer[MAX_ARGV];
/*
 ** ============================================================================
 **                       IMPLEMENTATIONS
 ** ============================================================================
 */

/*==============================================================================
 ** Function...: UART1Init
 ** Return.....: void
 ** Description: UART Initiation function
 ** Created....: 22.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void UART1Init(void) {
  TRISBbits.TRISB10 = 1;

  RPINR18bits.U1RXR = 10; //UART1 receive set to RB10
  RPOR5bits.RP11R   = 3; //UART1 transmit set to RB11
  U1BRG             = 8; // Initiate UART1 to 57600: ((4000000/57600)/8) - 1 = 8
  U1MODEbits.UARTEN = 1; // UART1 is Enabled
  U1MODEbits.USIDL  = 0; // Continue operation at Idlestate
  U1MODEbits.IREN   = 0; // IrDA En/Decoder is disabled
  U1MODEbits.RTSMD  = 1; // flow control mode
  U1MODEbits.UEN0   = 0; // Uart1 Tx and Rx pins are enabled and used.
  U1MODEbits.UEN    = 00;
  U1MODEbits.UEN1   = 0;
  U1MODEbits.WAKE   = 0; // Wake-up disabled
  U1MODEbits.LPBACK = 0; // Loop-back is disabled
  U1MODEbits.ABAUD  = 0; // auto baud is disabled
  U1MODEbits.RXINV  = 0; // No RX inversion
  U1MODEbits.BRGH   = 0; // low boud rate
  U1MODEbits.PDSEL  = 0b00; // 8bit no parity
  U1MODEbits.STSEL  = 0; // one stop bit

  U1STAbits.UTXISEL1 = 0; // Tx interrupt mode selection
  U1STAbits.URXISEL0 = 1; // Interrupt when a last character is shifted out of Transmit Shift Register. All Tx operations are complete.
  U1STA &= 0xDFFF; // clear TXINV by bit masking
  U1STAbits.UTXBRK   = 0; // sync break tx is disabled
  U1STAbits.UTXEN    = 1; //transmit is enabled
  U1STAbits.URXISEL  = 0b00; // interrupt flag bit is set when RXBUF is filled whit 1 character
  U1STAbits.ADDEN    = 0; // address detect mode is disabled

  IEC0bits.U1RXIE    = 1; // Enable uart receive interrupt.

  //reset RX interrupt flag
  IFS0bits.U1RXIF = 0;
}

void uart_print(char* ToBeSent) {
  putsUART1((unsigned int*) ToBeSent);
  while (BusyUART1());
}

void __attribute__((__interrupt__, auto_psv)) _U1RXInterrupt(void) {
  static unsigned char IntRcv = 0;
  USBbuffer[IntRcv] = ReadUART1();

  if (USBbuffer[IntRcv] == 10) {
    terminal(&USBbuffer);
    IntRcv = 0;
    memset(USBbuffer, '\0', sizeof (USBbuffer));
  } else {
    IntRcv++;
  }
  
  IEC0bits.U1RXIE = 1; 
  IFS0bits.U1RXIF = 0;
  return;
}
