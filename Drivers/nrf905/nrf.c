/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: nrf.c
        Version:   v0.0
        Date:      2012-08-09
    ============================================================================
 */

/*============================================================================*/
/*                           INCLUDE STATEMENTS
==============================================================================*/
#include <p24FJ64GB002.h>

#include "../driver.h"
#include "../../Includes/globalInclude.h"
#include "../../Protocol/dcf/dcf.h"

/*============================================================================*/
/*                           IMPLEMENTATION
==============================================================================*/

/**
 * Initializes the pins used by NRF905
 * @return 0 on success 1 otherwhys 
 */
int nrf(void)
{
    int ret = 0;

    TRISAbits.TRISA4 = 0; // Set A4 as TX_EN (OUTPUT))
    TRISBbits.TRISB2 = 1; // Set B2 as CD (INPUT))
    TRISBbits.TRISB3 = 1; // Set B3 as AM (INPUT))
    TRISBbits.TRISB4 = 1; // Set B4 as DR (INPUT))
    TRISBbits.TRISB7 = 0; // Set B7 as CSN (OUTPUT))
    TRISBbits.TRISB13 = 0; // Set B13 as PWR_UP (OUTPUT))
    TRISBbits.TRISB14 = 0; // Set B14 as TRX_CE (OUTPUT))
    RPINR0bits.INT1R = 4; //set RP4 (DR) to external interrupt 1

    // nRF initial modes
    TX_EN = 0; // Rx mode
    TRX_CE = 0; // Standby mode
    PWR_UP = 0; // Power down
    CSN = 0; // Chip Select Not = enabled

    // Initializes the external interrupt (From DR)
    INTCON2 = 0x0000; /* Setup interrupt on falling edge */
    IFS1bits.INT1IF = 0; /* Reset INT1 interrupt flag */
    IEC1bits.INT1IE = 1; /* Enable INT1 Interrupt Service Routine */
    IPC5bits.INT1IP = 1; /* Set low priority */

    ret = nrfConfiguration();

    return ret;
}

/**
 * Configuration of the nrf905 chip
 * @return 0 on success 1 otherwhys 
 */
int nrfConfiguration(void)
{
    int i;
    char *nRFAddress = "MOTE";
    unsigned char nRFConfig[10];

    nRFConfig[0] = CH_NO_BYTE; //Sets center frequency together with HFREQ_PLL
    //Output power, Band 433 or 868/915 MHz
    nRFConfig[1] = PA_PWR_10dBm | HFREQ_PLL_433MHz | CH_NO_BIT8;
    nRFConfig[2] = TX_AFW_4BYTE | RX_AFW_4BYTE; //Tx and Rx Address width
    nRFConfig[3] = RX_PW_4BYTE; //RX payload width
    nRFConfig[4] = TX_PW_4BYTE; //Tx payload width
    nRFConfig[5] = nRFAddress[0];
    nRFConfig[6] = nRFAddress[1];
    nRFConfig[7] = nRFAddress[2];
    nRFConfig[8] = nRFAddress[3];
    nRFConfig[9] = CRC8_EN | XOF_16MHz;
    //CRC check. Crystal oscillator frequency.

    PWR_UP = 1;
    CSN = 1;

    CSN = 0;
    SPI_wr(WC); //Write nRF configuration
    for (i = 0; i < 10; i++) {
        SPI_wr(nRFConfig[i]);
    }
    CSN = 1;

    CSN = 0;
    SPI_wr(WTA); //Write TX Address
    for (i = 0; i < 4; i++) {
        SPI_wr(nRFAddress[i]);
    }
    CSN = 1;

    TRX_CE = 1;

    return 0;
}

/**
 * Set payload width
 * @param type, 1 for TX mode 0 for RX mode
 * @param pw, the width of the payload 1-32 bytes
 */
void setNRFpw(int type, char pw)
{
    //configure RX payload width
    if (type == 0) {
        CSN = 0;
        SPI_wr(WC3); //Write to nRF configuration bit 3
        SPI_wr(pw);
        CSN = 1;
    }//configure TX payload width
    else if (type == 1) {
        CSN = 0;
        SPI_wr(WC4); //Write to nRF configuration bit 4
        SPI_wr(pw);
        CSN = 1;
    } // end else if
}

/**
 * Transmit the frame
 * @return 0 on success 1 otherwhys 
 */
int TXPacket(void)
{
    TRX_CE = 1; // Start transmitting
#ifndef SIMULATION 
    while (!DR);
#endif
    TX_EN = 0;
    START_TIK_TIMER

    return 0;
}

/**
 * Clock payload in to nrf905 register
 * @param data, the data to clock in
 * @param size, size of the data
 */
void clockIn(void* data, int size)
{
    int i;
    char *d = (char *) data;

    TRX_CE = 0;
    TX_EN = 1;
    CSN = 0;

    SPI_wr(WTP); //Write TX Payload

    for (i = 0; i < size; i++)
        SPI_wr(d[i]);
    CSN = 1;
}

/**
 * Clock payload out from nrf905 register
 * @param data, the data to clock out
 * @param size, size of the data
 */
void clockOut(void* data, int size)
{
    int i;
    char *d = (char *) data;

    TRX_CE = 0;
    CSN = 0;
    SPI_wr(RRP); //Read RX Payload
    for (i = 0; i < size; i++)
        d[i] = SPI_wr(0);
    CSN = 1;
    TRX_CE = 1;
}

/**
 * Transmit the data using the NRF905 chip
 * @param data, payload that will be transmitted
 * @param pw, size of the payload
 * @return 0 on success 1 otherwhys
 */
int transmitFrame(void* data, char pw)
{
    int ret = 0;
    
    setNRFpw(1, pw); // Set the TX payload width
    clockIn(data, pw); // Clock data in with SPI
    IEC1bits.INT1IE = 0; //Disabling external interrupt before tx
    ret = TXPacket(); // Transmit
    IFS1bits.INT1IF = 0; /* Reset INT1 interrupt flag */
    IEC1bits.INT1IE = 1; //Enabling external interrupt after tx

    return ret;
}

/**
 * External interrupt for data ready, sat by nrf905
 */
void __attribute__((__interrupt__, auto_psv)) _INT1Interrupt(void)
{
    IFS1bits.INT1IF = 0; //Clear the INT1 interrupt flag
    char s[32];     // FIXME_MAGIC_NUMBER
    
    uint8_t fr[32] = {0};  
    
    
    
    clockOut(&fr, 4);
            

    
    if(Mlme.mAddr == fr[0])
    {
        sprintf(s,"Got ACK\n");
        uart_print(s); 
        Mlme.mGotACK = YES;
    }
    else
    {
        sprintf(s,"Got ACK Not for me\n");
        uart_print(s);
    }
    
   
    
     
}
