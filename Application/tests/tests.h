/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: test.h
        Version:   v0.0
        Date:      2012-08-09
    ============================================================================
 */

/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#ifndef TESTS_H
#define	TESTS_H




/*
** =============================================================================
**                       EXPORTED FUNCTION DECLARATION
** =============================================================================
*/

/**
 * Test cases suite for UART
 */
void app_uart_test(void);


/**
 * Test cases suite for Nrf905
 */
void app_nrf905_test(void);


/**
 * Test cases suite for SPI
 */
void app_spi_test(void);

/**
 * Test cases suite for Timer
 */
void app_timer_test(void);

/**
 * Test cases suite for DCF protocol
 */
void app_dcf_test(void);

#endif	/* TESTS_H */


