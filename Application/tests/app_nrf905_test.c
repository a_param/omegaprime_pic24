/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: app_nrf905_test.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include <string.h>

#include "tests.h"
#include "../../Drivers/driver.h"
#include "../../Includes/globalInclude.h"
#include "../../Protocol/protocol.h"




/*
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */


/*==============================================================================
 ** Function...: app_nrf905_test
 ** Return.....: void
 ** Description: Testing the basic functionalities of Nrf905
 ** Created....: 23.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void app_nrf905_test(void)
{
    START_ONE_MILI_TIMER;
    char tf[32];

    tf[0]  = 0x00; 
    tf[1]  = 0x01;
    tf[2]  = 0x02;
    tf[3]  = 0x03;
    tf[4]  = 0x04;
    tf[5]  = 0x05;
    tf[6]  = 0x06;
    tf[7]  = 0x07;
    tf[8]  = 0x08;
    tf[9]  = 0x09; 
    tf[10] = 0x0A;
    tf[11] = 0x0B;
    tf[12] = 0x0C;
    tf[13] = 0x0D;
    tf[14] = 0x0E;
    tf[15] = 0x0F;
    tf[16] = 0x10;
    tf[17] = 0x11; 
    tf[18] = 0x12;
    tf[19] = 0x13;
    tf[20] = 0x14;
    tf[21] = 0x15;
    tf[22] = 0x16;
    tf[23] = 0x17;
    tf[24] = 0x18;
    tf[25] = 0x19; 
    tf[26] = 0x1A;
    tf[27] = 0x1B;
    tf[28] = 0x1C;
    tf[29] = 0x1D;
    tf[30] = 0x1E;
    tf[31] = 0x1F;

   // unsigned int cnt = 0;
    

    while(1)
    {
        if(CD)
        {
            LED_RED_1_TOGGLE;
        }
        else
        {
            LED_GREEN_1_TOGGLE;
        }
        
        /*
        if(TIMER_ONE_SEC < oneMilliTimer ) 
        {
            LED_GREEN_1_TOGGLE;
                        
            memcpy(tf, &cnt, 2);
            uart_print(" -------- Send --------\n");
            transmitFrame(&tf, PACKET_SIZE);

            cnt++;
            
           START_ONE_MILI_TIMER;
        }
         * */
    }
}