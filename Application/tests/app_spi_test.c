/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    ============================================================================
    This document contains proprietary information belonging to Achuthan 
    Paramanathan. Passing on and copying of this document, use and communication 
    of its contents is not permitted without prior written authorisation.
    ============================================================================
    Revision Information:
        File name: app_spi_test.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include "tests.h"
#include "../../Drivers/driver.h"

/*
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */


/*==============================================================================
 ** Function...: app_spi_test
 ** Return.....: void
 ** Description: Testing the basic functionalities of SPI
 ** Created....: 23.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void app_spi_test(void)
{
    
}