/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    ============================================================================
    This document contains proprietary information belonging to Achuthan 
    Paramanathan. Passing on and copying of this document, use and communication 
    of its contents is not permitted without prior written authorisation.
    ============================================================================
    Revision Information:
        File name: app_uart_test.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include "tests.h"
#include "../../Drivers/driver.h"

/*
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */


/*==============================================================================
 ** Function...: app_uart_test
 ** Return.....: void
 ** Description: Testing the basic functionalities of UART
 ** Created....: 23.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void app_uart_test(void)
{
    //UART1Init();
    uart_print("UART Initialized\n");

    
}