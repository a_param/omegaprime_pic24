/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: app_nrf905_test.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include "tests.h"
#include "../../Drivers/driver.h"
#include "../../Includes/globalInclude.h"
#include "../../Protocol/protocol.h"
#include "../../System/system.h"


/*
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */
void dcf_app_interface(uint8_t dcfTask);
void app_incomming(void);
void app_outgoing(void);
void dataRateTest(void);

/*
 ** ============================================================================
 **                   LOCAL VARIABLES
 ** ============================================================================
 */

uint8_t dcfTask = 0;
unsigned int pktTxCnt = 0;

/*==============================================================================
 ** Function...: app_dcf_test
 ** Return.....: void
 ** Description: Testing the basic functionalities of DCF protocol
 ** Created....: 29.12.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void app_dcf_test(void)
{
    //START_ONE_MILI_TIMER;
    
    /* initialize function pointer to dataRateTest */
    cb_timer ptr_dataRateTest_cb = dataRateTest;
    /* register our callback function */
    register_timer_cb(ptr_dataRateTest_cb); 
    
    while(1)
    {
        dcf_app_interface(dcfTask);
        dcfTask++;
        if(dcfTask>2) dcfTask = 0;
        
        /*
        if( TIMER_ONE_SEC < oneMilliTimer ) 
        {
            uint8_t *data;
            
            data = (uint8_t *)malloc(24);          
            
            memcpy(data, &cnt,2);
            memcpy(data +2, "1234567890abcdefghijkl", 22);
            //op_hexdump(1, "----- APP LAYER ----", data, 24);
            network_interface(OUTGOING, 64, 24, data);
            
            if (! buffer_full(&rBuf))
            {
                write_buffer(data, &rBuf);
            }
            else
            {
                uart_print("Buffer full\n");
            }
            
            
            free(data);
            
            START_ONE_MILI_TIMER;
            cnt++;
        }
        */
    }
}


/*==============================================================================
** Function...: dcf_app_interface
** Return.....: GLOB_RET
** Description: main application layer
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void dcf_app_interface(uint8_t dcfTask)
{
    switch(dcfTask)
    {
        case INCOMING:
            app_incomming();
            break;

        case  OUTGOING:
            app_outgoing();
            break;

        default:
            break;
    }
}


/*==============================================================================
** Function...: app_incomming
** Return.....: void
** Description: private function that handles all incomming packets
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void app_incomming(void)
{
    
}


/*==============================================================================
** Function...: app_outgoing
** Return.....: void
** Description: private function that handles all outgoing packets
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/

void app_outgoing(void)
{
    
    if (!buffer_empty(&rBuf))
    {
        uint8_t *data;
        data = (uint8_t *)malloc(24);  
        
        read_buffer(data, &rBuf);
        
        network_interface(OUTGOING, 255, 24, data);
        
        free(data);
    }
    
}

/*==============================================================================
** Function...: dataRateTest
** Return.....: GLOB_RET
** Description: function that generates packets for output. This function is
 * called from time interrupt accoring to the user sat data rate.
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void dataRateTest(void)
{
    uint8_t *data;

    data = (uint8_t *)malloc(24);          

    memcpy(data, &pktTxCnt,2);
    memcpy(data +2, "1234567890abcdefghijkl", 22);
 
    if (! buffer_full(&rBuf))
    {
        write_buffer(data, &rBuf);
    }
    else
    {
        uart_print("Buffer full\n");
    }
    pktTxCnt++;       
    free(data);
}
