/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: dcf.c
        Version:   v0.0
        Date:      2015-12-28
    ============================================================================
 */


/*
** =============================================================================
**                        INCLUDE STATEMENTS
** =============================================================================
*/

#include "dcf.h"


/*
** =============================================================================
**                   LOCAL FUNCTION DECLARATIONS
** =============================================================================
*/

GLOB_RET backOff(void);
void selectBackoff(void);
void selectCW(void);
GLOB_RET basicDCF(void *frame);
GLOB_RET mac_outgoing(unsigned char addr, unsigned char len, void *frame );
GLOB_RET mac_incoming(void *frame);

/*
** ==========================================================================
**                       Layer specific variables
** ==========================================================================
*/

mlme Mlme;


/*
** =============================================================================
**                    FUNCTIONS
** =============================================================================
*/


/*==============================================================================
** Function...: dcfInit
** Return.....: GLOB_RET
** Description: dataLink
** Created....: 24.08.2012 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void dcfInit(void)
{
    Mlme.headerSize = 4;
    Mlme.mAddr = 2;
    Mlme.mACKTimeOut = 200;
    
    Mlme.mBackoffTimer = 0;
    Mlme.mCWmax = 128;
    Mlme.mCWmin = 8;
    Mlme.mCurrentCW = Mlme.mCWmin;
    Mlme.mExpectACK = NO;
    Mlme.mExpectPAY = YES;
    Mlme.mGotACK = NO;
    Mlme.mGotPAY = NO;
    Mlme.mMACdrop = 6;
    Mlme.mPAYTimeOut = 84;
    Mlme.mSIFSTime = 8;
    Mlme.mSlotTime = 8;
    Mlme.mDIFSTtime = 24;
    Mlme.mBcast = YES;

}

/*==============================================================================
** Function...: channelAccess
** Return.....: GLOB_RET
** Description: dataLink
** Created....: 24.08.2012 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/

GLOB_RET channelAccess(void *frame, unsigned char retCode)
{
    GLOB_RET ret = GLOB_FAILURE;
    
    switch (retCode)
    {
        //Wait DIFS period, if there is CD then freeze the counter
        case WAIT_DIFS:
            START_DIFS_TIMER;
            ret = DATA_LINK_DIFS_WAIT_OK;
            while( difsTimer < Mlme.mDIFSTtime)
            {
                if(CD)
                { 
                    while(CD)
                    {
                        //FIXME: can do something use full, instead of wile? 
                    }
                }
            }
            break;
            
        case PERFORM_BACKOFF:
            if(DATA_LINK_CARRIER_DETECTED==backOff())
            {
                //if CD then return:
                return DATA_LINK_CARRIER_DETECTED;
            }
            else
            {
                //else no CD then do the transmit 
                ret = DATA_LINK_BACKOFF_OK;
            }
            break;
            
        case SEND_PAY:
            ret = transmitFrame(frame, PACKET_SIZE);
            break;
            
        case WAIT_FOR_ACK:
            START_ACK_TIMER;
            //Block until ACK is received or timeout
            while( ackTimer < Mlme.mACKTimeOut && Mlme.mGotACK == NO );
             // ACK received ?
            if(Mlme.mGotACK)
            {
                ret = DATA_LINK_GOT_ACK;
                LED_GREEN_1_TOGGLE;
            }
            else
            {
                ret = DATA_LINK_ACK_TIMEOUT;
            }
            
            break;
            
        default:
            ret = GLOB_ERROR_INVALID_PARAM;
            break; 
    }
    
    return ret;
}


/*==============================================================================
** Function...: basicDCF
** Return.....: GLOB_RET
** Description: dataLink
** Created....: 24.08.2012 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/

GLOB_RET basicDCF(void *frame)
{
    GLOB_RET ret = GLOB_FAILURE;
    
    // Wait DIFS
    ret = channelAccess(frame, WAIT_DIFS);
    if(DATA_LINK_DIFS_WAIT_OK == ret)
    {
        // Select a new backoff window if the backofftimer is == 0
        if(Mlme.mBackoffTimer == 0) selectBackoff();
        ret = channelAccess(frame, PERFORM_BACKOFF);
        if(DATA_LINK_BACKOFF_OK==ret)
        {
            // send the payload
            channelAccess(frame, SEND_PAY);
            // now we have transmitted a PAYload and we don't expect a payload
            Mlme.mExpectPAY = NO;
            // we  expect an ack
            Mlme.mExpectACK = YES;
            // reconf. the nrf driver to receive an ACK packet (size of header) 
            setNRFpw(RX, HEADER_SIZE);
            // wait for ACK if not broadcast
            if(Mlme.mBcast)
            {
                LED_GREEN_1_TOGGLE;
                ret = DATA_LINK_GOT_ACK;
            }
            else 
            {
                ret = channelAccess(frame, WAIT_FOR_ACK);
            }
            // we got ack
            if(DATA_LINK_GOT_ACK == ret)
            {
                //set the CW size to the default min
                Mlme.mCurrentCW = Mlme.mCWmin;
                ret = GLOB_SUCCESS;
            }
            //no ack/ack timed out
            else
            {
                selectCW();
                ret = DATA_LINK_ACK_TIMEOUT;
            }
        }
        else
        {
            ret = DATA_LINK_CARRIER_DETECTED;
        }
    }
    
    Mlme.mExpectPAY = YES;
    Mlme.mExpectACK = NO;
    Mlme.mGotACK = NO;
    
    setNRFpw(RX, PACKET_SIZE);
    return ret;
}


/*==============================================================================
** Function...: mac_interface
** Return.....: GLOB_RET
** Description: medium layer interface. All call must go through this interface
** Created....: 04.04.2013 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
GLOB_RET mac_interface(char iKey, char addr, char len, void *frame)
{
    GLOB_RET ret = GLOB_SUCCESS;

    if(OUTGOING==iKey)
    {
        ret = mac_outgoing(addr, len, frame);
    }
    else if (INCOMING == iKey)
    {
        ret = mac_incoming(frame);
    }
    else
    {
        ret = GLOB_ERROR_INVALID_PARAM;
    }
    return ret;
}


/*==============================================================================
** Function...: mac_incoming
** Return.....: GLOB_RET
** Description: private function that handles all incomming packets
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
GLOB_RET mac_incoming( void *frame)
{
    GLOB_RET ret = GLOB_SUCCESS;
    
    return ret;
}


/*==============================================================================
** Function...: sendPacket
** Return.....: GLOB_RET
** Description: session
** Created....: 09.08.2012 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/

GLOB_RET mac_outgoing(uint8_t addr, uint8_t len, void *frame )
{
    GLOB_RET ret;
    int i = 0; // MAC retrans. counter
    uint8_t *macFrame;
    char macFrameSize = len + Mlme.headerSize;
    char seq = 0, con = 0;
    char s[30];
    
    macFrame = (uint8_t *)malloc(macFrameSize);
    
    memcpy(macFrame, &addr, 1);             // Dest
    memcpy(macFrame + 1, &Mlme.mAddr, 1);   // Src
    memcpy(macFrame + 2, &seq, 1);          // Sec
    memcpy(macFrame + 3, &con, 1);          // Control
    memcpy(macFrame + 4, frame, len);       // Data
    
    //op_hexdump(1, "----- MAC LAYER ----", macFrame, 32);
    
    if(255==addr)
    {
        Mlme.mBcast = YES;
    }
    else
    {
        Mlme.mBcast = NO;
    }

    while( i < Mlme.mMACdrop) //re-Send the packet i'th times before dropping it
    {
        ret = basicDCF(macFrame); // Simple CSMA/CA MAC
        // If Carrier detect wait untill !CD
        if(DATA_LINK_CARRIER_DETECTED == ret) while(CD);
        // if ret = no ack then we do retransmission
        else if(DATA_LINK_ACK_TIMEOUT == ret)
        {
             i++; // MAC retrans. counter
        }
        // if ret = success, we reset the contention window cw to initial val.
        else if(GLOB_SUCCESS == ret)
        {
            // Setting contention window to initial val.
            Mlme.mCurrentCW = Mlme.mCWmin;      
            free(macFrame);
            return GLOB_SUCCESS; // return to the caller with sucess    
        }
        // The tranmission is not a success we reset the contention window
    }
    Mlme.mCurrentCW = Mlme.mCWmin;
    LED_RED_1_TOGGLE;
    
    sprintf(s,"ACK TIMEOUT ret = %d\n", ret);
    uart_print(s);
    
    free(macFrame);
    return DATA_LINK_TRANSMISSION_FAILED;
}


/*==============================================================================
** Function...: backOff
** Return.....: GLOB_RET
** Description: dataLink
** Created....: 24.08.2012 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/

GLOB_RET backOff(void)
{
    START_SLOT_TIMER;
    // countdown to 0 while no CD if CD return 1 (CD detected)
    while(!CD)
    {
        if(slotTimer >= Mlme.mSlotTime)
        {
            Mlme.mBackoffTimer--; 
            START_SLOT_TIMER;
        }
        if(Mlme.mBackoffTimer == 0)  // FIXME Magic number
        {
            return DATA_LINK_BACKOFF_OK;
        }
    }
    return DATA_LINK_CARRIER_DETECTED;    
}


/*==============================================================================
** Function...: selectBackoff
** Return.....: void
** Description: dataLink
** Created....: 24.08.2012 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/

void selectBackoff(void)
{
  //char s[90];
  
   srand(Mlme.mAddr);
  // get a random number within [0-cw]
  Mlme.mBackoffTimer = ( rand() % Mlme.mCurrentCW ) + 1; 
  //sprintf(s,"selectBackoff | Mlme.mBackoffTimer %d\n", Mlme.mBackoffTimer);
  //uart_print(s);
}


/*==============================================================================
** Function...: selectCW
** Return.....: void
** Description: dataLink
** Created....: 24.08.2012 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/

void selectCW(void)
{
  //char s[90];
  if( Mlme.mCurrentCW< Mlme.mCWmax )
  {
    Mlme.mCurrentCW = 2*Mlme.mCurrentCW; 
    //sprintf(s,"selectCW | Mlme.mCurrentCW %d\n", Mlme.mCurrentCW);
    //uart_print(s);
  }
}