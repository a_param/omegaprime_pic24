/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: dcf.h
        Version:   v0.0
        Date:      2012-08-09
    ============================================================================
 */

/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#ifndef DCF_H
#define	DCF_H

#include <stdlib.h> 
#include <math.h>

#include "../../Includes/globalInclude.h"
#include "../../Drivers/driver.h"


/*
** =============================================================================
**                   GLOBAL EXPORTED FUNCTION DECLARATIONS
** =============================================================================
*/


/**
 * Description:
 * @param:
 * @return:
*/
int dcf_backOff(void);


/*
** ==========================================================================
**                        DEFINES and ENUMS 
** ==========================================================================
*/


#define START_SLOT_TIMER   slotTimer  = 0;
#define START_DIFS_TIMER   difsTimer  = 0;
#define START_ACK_TIMER    ackTimer   = 0;


/**
 * Enumeration containing the commands for channel access
*/
typedef enum
{
    WAIT_DIFS = 0,
    PERFORM_BACKOFF = 1,
    SEND_RTS = 2,
    WAIT_FOR_CTS = 3,
    SEND_PAY = 4,
    WAIT_FOR_ACK = 5
}CSMA_CMD;

typedef struct{
    char headerSize;
    char mBcast;
    char mAddr;
    char mSlotTime;
    char mSIFSTime;
    char mDIFSTtime;
    char mCWmin;
    char mCWmax;
    char mCurrentCW; 
    char mMACdrop ;
    char mPAYTimeOut;
    unsigned int mACKTimeOut;
    int  mBackoffTimer;
    char mGotACK;
    char mGotPAY;
    char mExpectPAY;
    char mExpectACK;
}mlme;


/*
 ** ==========================================================================
 **                       Extern Global variables
 ** ==========================================================================
 */

extern mlme Mlme;

extern unsigned int txPktTimer;
extern unsigned int slotTimer;
extern unsigned int difsTimer;
extern unsigned int ackTimer;


#endif //DCF_H