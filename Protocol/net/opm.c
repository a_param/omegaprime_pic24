/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: opm.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */


#include <string.h>

#include "../../Drivers/driver.h"
#include "../../Includes/globalInclude.h"
#include "../../Protocol/protocol.h"


/*
 ** ============================================================================
 **                   LOCAL  FUNCTION DECLARATIONS
 ** ============================================================================
 */

GLOB_RET network_outgoing( uint8_t addr, uint8_t len, void *frame );
GLOB_RET network_incoming( void *frame );
char findNextHop(void);

/*
** ==========================================================================
**                       Layer specific variables
** ==========================================================================
*/

nlme Nlme;


/*
** =============================================================================
**                    FUNCTIONS
** =============================================================================
*/

/*==============================================================================
** Function...: opmInit
** Return.....: GLOB_RET
** Description: network layer initialization
** Created....: 29.12.2012 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void opmInit(void)
{
    Nlme.ttl = 7;
    Nlme.headerSize = 4;
    Nlme.networkSize = 20;

}

/*==============================================================================
** Function...: network_layer
** Return.....: GLOB_RET
** Description: network layer interface. All call must go thoug this interface
** Created....: 04.04.2013 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/

GLOB_RET network_interface(char iKey, uint8_t addr, uint8_t len, void *frame)
{
    GLOB_RET ret = GLOB_SUCCESS;

    if(OUTGOING==iKey)
    {
        ret = network_outgoing(addr, len, frame); 
    }
    else if (INCOMING == iKey)
    {
        ret = network_incoming(frame);
    }
    else
    {
        ret = GLOB_ERROR_INVALID_PARAM;
    }
    return ret;
}


/*==============================================================================
** Function...: network_outgoing
** Return.....: GLOB_RET
** Description: private function that handles all outgoing packets
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
GLOB_RET network_outgoing( uint8_t addr, uint8_t len, void *frame )
{
    GLOB_RET ret = GLOB_SUCCESS;
    
    uint8_t *netFrame;
    uint8_t netFrameSize = len + Nlme.headerSize;
    uint8_t con = 0, rsv = 0, nextDest = 0;
    
    netFrame = (uint8_t *)malloc(netFrameSize);
    
    memcpy(netFrame, &Nlme.ttl, 1); // TTL
    memcpy(netFrame+1, &addr, 1);   // mesh dest
    memcpy(netFrame+2, &con, 1);    // con
    memcpy(netFrame+3, &rsv, 1);    // rsv
    memcpy(netFrame+4, frame, len);
    
    //op_hexdump(1, "----- NET LAYER ----", netFrame, netFrameSize);

    // Add MAC / MESH header
    nextDest = findNextHop();
    
    mac_interface(OUTGOING, nextDest,  netFrameSize, netFrame);
    
    free(netFrame);
    
    return ret;
}


/*==============================================================================
** Function...: network_incoming
** Return.....: GLOB_RET
** Description: private function that handles all incomming packets
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
GLOB_RET network_incoming( void *frame)
{
    GLOB_RET ret = GLOB_SUCCESS;
    
    return ret;
}


/*==============================================================================
** Function...: findNextHop
** Return.....: char
** Description: private function that find the next hop.
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
char findNextHop(void)
{

    return 255;
    
}